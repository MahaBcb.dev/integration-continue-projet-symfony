<?php

namespace App\Tests;

use App\Entity\Demo;
use PHPUnit\Framework\TestCase;

class DemoUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $demo = new Demo;
        $demo->setDemo('demo');

        $this->assertTrue($demo->getDemo() === 'demo');
    }
}
